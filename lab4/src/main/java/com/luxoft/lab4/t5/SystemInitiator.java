package com.luxoft.lab4.t5;

import java.util.List;

import com.luxoft.lab4.t5.configuration.FIXApplicationConfiguration;
import com.luxoft.lab4.t5.configuration.SeasonedApplicationConfiguration;

/**
 * Created by aniamamam on 2014-04-30.
 */
public class SystemInitiator {
    private ConfigurationReader configurationReader;

    private ApplicationConfigurator configurator;

    public void configureAll() {
        List<FIXApplicationConfiguration> fixConfig = configurationReader.readFixApplicationConfigs();
        List<SeasonedApplicationConfiguration> seasonedConfig = configurationReader.readSeasonedApplicationConfigs();

         configurator.configure(fixConfig);
         configurator.configure(seasonedConfig);
    }

    public void setConfigurator(ApplicationConfigurator configurator) {
        this.configurator = configurator;
    }

    public void setConfigurationReader(ConfigurationReader configurationReader) {
        this.configurationReader = configurationReader;
    }

}
