package com.luxoft.lab4.t5;

import java.util.List;

import com.luxoft.lab4.t5.configuration.ApplicationConfiguration;

/**
 * Created by aniamamam on 2014-04-30.
 */
public class ApplicationConfigurator {
    private List<Application> applications;


    public void setApplications(List<Application> applications) {
        this.applications = applications;
    }


public void configure(List<? extends ApplicationConfiguration> configurations) {
        for (ApplicationConfiguration config : configurations) {
            for (Application application: applications) {
                config.configure(application);
            }
        }
    }



}
