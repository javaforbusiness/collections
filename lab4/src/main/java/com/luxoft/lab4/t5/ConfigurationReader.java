package com.luxoft.lab4.t5;

import java.util.List;

import com.luxoft.lab4.t5.configuration.FIXApplicationConfiguration;
import com.luxoft.lab4.t5.configuration.SeasonedApplicationConfiguration;

/**
 * Created by aniamamam on 2014-04-30.
 */
public abstract class ConfigurationReader {
    public abstract List<FIXApplicationConfiguration> readFixApplicationConfigs();
    public abstract List<SeasonedApplicationConfiguration> readSeasonedApplicationConfigs();
}
