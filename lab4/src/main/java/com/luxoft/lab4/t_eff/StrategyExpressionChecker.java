package com.luxoft.lab4.t_eff;

import java.util.Arrays;
import java.util.List;

/**
 * @author jaworskp
 */
public class StrategyExpressionChecker implements ExpressionChecker {

    final private Or expr =
        new Or(new Equals("aaaaa"), new Equals("bbbbb"), new Equals("ccccc"), new Equals("ddddd"), new Equals("zzzzz"));

    @Override
    public boolean check(Message msg) {

        return expr.check(msg.field1);
    }

    static interface Strategy {

        public boolean check(String msg);
    }

    static class Equals implements Strategy {

        String exp;

        public Equals(String expected) {

            this.exp = expected;
        }

        public boolean check(String whatToCheck) {

            return exp.equals(whatToCheck);
        }
    }

    static class Or implements Strategy {

        List<Strategy> strategiesOr;

        public Or(Strategy... strategies) {

            strategiesOr = Arrays.asList(strategies);
        }


        @Override
        public boolean check(String msg) {

            for (Strategy s : strategiesOr) {
                if (s.check(msg)) {
                    return true;
                }
            }
            return false;

        }
    }

}
