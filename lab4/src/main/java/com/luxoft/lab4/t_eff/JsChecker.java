package com.luxoft.lab4.t_eff;

import javax.script.Bindings;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleBindings;

/**
 * @author jaworskp
 */
public class JsChecker implements ExpressionChecker {

    private final ScriptEngine engine;
    private final Bindings bindings = new SimpleBindings();
    private final CompiledScript cs;

    {
        try {
            ScriptEngineManager factory = new ScriptEngineManager();
            engine = factory.getEngineByName("JavaScript");
            cs = ((Compilable)engine).compile("function check(msg) {return msg == 'aaaaa' || msg == 'bbbbb' || msg == 'ccccc' || msg == 'ddddd' || msg == 'zzzzz'}");
        } catch (ScriptException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public boolean check(Message msg) {
        bindings.put("msg", msg.field1);

        try {
            Boolean res = (Boolean) cs.eval(bindings);
            return res;
        } catch (ScriptException e) {
            throw new RuntimeException(e);
        }

    }
}
