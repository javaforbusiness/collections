package com.luxoft.lab4.t_eff;

/**
 * @author jaworskp
 */
public class JavassistExpressionChecker implements ExpressionChecker {

    @Override
    public boolean check(Message msg) {
        if (msg.field1.equals("aaaaa") || msg.field1.equals("bbbbb") || msg.field1.equals("ccccc") || msg.field1.equals("ddddd") || msg.field1.equals("zzzzz")) {
            return true;
        }
        return false;
    }
}

