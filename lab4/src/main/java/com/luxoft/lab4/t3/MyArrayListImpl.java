package com.luxoft.lab4.t3;

import java.util.AbstractList;

/**
 * Created by aniamamam on 2014-04-24.
 */
public class MyArrayListImpl<T> extends AbstractList<T> implements MyArrayList<T> {

	int size;
	Object[] arr;
	
    public MyArrayListImpl(int size) {
    	arr = new Object[size];
    	this.size = 0;
    }

    @SuppressWarnings("unchecked")
	@Override
    public T get(int index) {
    	if(index >= size) 
    		throw new IndexOutOfBoundsException();
        return (T)arr[index];
    }

    @Override
    public int size() {
        return size;
    }
    
    @Override
    public void add(int index, T t){
    	arr[index] = t;
    	size++;
    }
}
