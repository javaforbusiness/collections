package com.luxoft.lab4.t1;

import java.util.LinkedList;

/**
 * Created by aniamamam on 2014-04-24.
 */
public interface FIFOQueue<T> {
	

    T poll();

    T peek();

    void put(T t);
}
