package com.luxoft.lab4.t1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by aniamamam on 2014-04-25.
 */
public class LIFOQueueImpl<T> implements LIFOQueue<T> {

	LinkedList<T> list = new LinkedList<T>();
	
    @Override
    public T poll() {
    	return list.pollLast();
    }

    @Override
    public T peek() {
    	return list.peekLast();
    }

    @Override
    public void put(T t) {
    	list.add(t);
    }
}
