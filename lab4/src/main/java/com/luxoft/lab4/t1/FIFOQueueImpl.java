package com.luxoft.lab4.t1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by aniamamam on 2014-04-24.
 */
public class FIFOQueueImpl<T> implements FIFOQueue<T> {

	LinkedList<T> list = new LinkedList<T>();
	
	
	
	@Override
	public T poll() {
		return list.pollFirst();
	}

	@Override
	public T peek() {
		return list.peekFirst();
	}

	@Override
	public void put(T t) {
		list.add(t);
	}
}
