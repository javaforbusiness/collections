package com.luxoft.lab4;

public interface MessageSource {
    public Message next();
}
