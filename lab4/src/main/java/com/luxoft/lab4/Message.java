package com.luxoft.lab4;

import java.io.Serializable;

public class Message implements Serializable {

    private static final long serialVersionUID = 429232152386347L;

    private String applID;
    private String secondaryExecID;
    private String side;
    private String price;

    public String getApplID() {

        return applID;
    }

    public void setApplID(String applID) {

        this.applID = applID;
    }

    public String getSecondaryExecID() {

        return secondaryExecID;
    }

    public void setSecondaryExecID(String secondaryExecID) {

        this.secondaryExecID = secondaryExecID;
    }

    public String getSide() {

        return side;
    }

    public void setSide(String side) {

        this.side = side;
    }

    public String getPrice() {

        return price;
    }

    public void setPrice(String price) {

        this.price = price;
    }
}
