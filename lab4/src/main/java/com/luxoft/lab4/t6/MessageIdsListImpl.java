package com.luxoft.lab4.t6;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by aniamamam on 2014-04-23.
 */
public class MessageIdsListImpl implements MessagesIds {
    private Logger logger = LoggerFactory.getLogger(MessageIdsListImpl.class);

    private int[] messageIds = new int[1000000];
    int size;

    public void addId(int id) {
        messageIds[size] = id;
        size++;
    }

    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            int pos = 0;

            @Override
            public boolean hasNext() {
                return pos < size;
            }

            @Override
            public Integer next() {
                Integer messageId = messageIds[pos++];
                if (logger.isDebugEnabled()) {
                    logger.debug("Next messageId=" + messageId);
                }
                return messageId;
            }
            @Override
            public void remove() {
            	// TODO Auto-generated method stub
            	
            }
        };
    }
}
