package com.luxoft.lab4.t3;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Created by aniamamam on 2014-04-24.
 */
public class MyArrayListImplTest {
    @Test
    public void testIt() {
        MyArrayList<Integer> list = new MyArrayListImpl<>(10);
        for (int i=0; i<10; i++) {
            list.add(30-i);
        }

        for (int i=0; i<10; i++) {
            assertEquals(Integer.valueOf(30 - i), list.get(i));
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testDoesNotGrow() {
        MyArrayList<Integer> list = new MyArrayListImpl<>(10);
        for (int i=0; i<11; i++) {
            list.add(i);
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetOverflow() {
        MyArrayList<Integer> list = new MyArrayListImpl<>(10);
        list.add(0);
        list.add(1);
        list.get(2);
    }

}
