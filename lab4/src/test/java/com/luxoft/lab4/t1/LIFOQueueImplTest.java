package com.luxoft.lab4.t1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.luxoft.lab4.utils.EfficiencyTestUtil;

/**
 * Please implement the LIFOQueueImpl using any java collection as a composite, for good time efficiency.
 */
public class LIFOQueueImplTest extends EfficiencyTestUtil {

    @Test
    public void runEfficiencyTest() throws Exception {
        runTestTimeConstraint(15000);
    }

    public static void main(String[] args) {
        LIFOQueue<Integer> lifo = new LIFOQueueImpl<>();
        for (int i=0; i<1000000; i++) {
            lifo.put(i);
        }
        for (int i=999999; i>=0; i--) {
            assertEquals(Integer.valueOf(i), lifo.poll());
        }
        assertEquals(null, lifo.peek());
    }
}
