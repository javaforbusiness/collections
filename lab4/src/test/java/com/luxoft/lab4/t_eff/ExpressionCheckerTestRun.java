package com.luxoft.lab4.t_eff;

import java.util.ArrayList;
import java.util.List;

public class ExpressionCheckerTestRun {

    public static void main(String[] args) {

        final int msgLength = 5;
        final int numberOfCharacters = 'z' - 'a' + 1;

        int numOfMessages = pow(numberOfCharacters, msgLength);
        List<Message> messages = new ArrayList<>(numOfMessages);

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numOfMessages; i++) {
            int ii = i;
            for (int c = 0; c < msgLength; c++) {
                sb.append((char) (ii % numberOfCharacters + 'a'));
                ii = ii / numberOfCharacters;
            }
            messages.add(new Message(sb.toString()));
            sb.setLength(0);
        }

        ExpressionChecker javassistChecker = new JavassistExpressionChecker();

        ExpressionChecker strategyChecker = new StrategyExpressionChecker();

        testAlgorithm(messages, javassistChecker);
        testAlgorithm(messages, strategyChecker);
        testAlgorithm(messages, javassistChecker);
        testAlgorithm(messages, strategyChecker);
        testAlgorithm(messages, javassistChecker);
        testAlgorithm(messages, strategyChecker);
        testAlgorithm(messages, javassistChecker);
        testAlgorithm(messages, strategyChecker);
        testAlgorithm(messages, javassistChecker);
        testAlgorithm(messages, strategyChecker);

        testAlgorithm(messages, javassistChecker);
        testAlgorithm(messages, strategyChecker);
        testAlgorithm(messages, javassistChecker);
        testAlgorithm(messages, strategyChecker);
        testAlgorithm(messages, javassistChecker);
        testAlgorithm(messages, strategyChecker);
        testAlgorithm(messages, javassistChecker);
        testAlgorithm(messages, strategyChecker);


        System.err.println("");
        System.err.println("");

        testAlgorithm(messages, javassistChecker);
        testAlgorithm(messages, strategyChecker);
    }

    private static void testAlgorithm(List<Message> messages, ExpressionChecker checker) {

        long start = System.currentTimeMillis();
        int found = 0;
        for (Message msg : messages) {
            if (checker.check(msg)) {
                found++;
            }

        }
        long end = System.currentTimeMillis();

        System.err.println(
            checker.getClass().getSimpleName() + ": Found " + found + " matches amongst " + messages.size() +
                " messages in time " + (end - start) + "ms");
    }

    private static int pow(int base, int wykladnik) {

        int result = 1;
        for (int i = 0; i < wykladnik; i++) {
            result *= base;
        }
        return result;
    }


}
