package com.luxoft.lab4.experimental;

import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by aniamamam on 2014-04-25.
 */
@RunWith(MemEfficiencyRunner.class)
public class T1 {
    @EfficiencyTest(heapMaxMb = 10)
    @Test
    public void test1() throws Exception {
        System.err.println("Running test 1");
        Thread.sleep(3000);
    }

    @EfficiencyTest(heapMaxMb = 10)
    @Test
    public void test2() throws Exception {
        System.err.println("Running test 2");
        Thread.sleep(2000);
    }

}
