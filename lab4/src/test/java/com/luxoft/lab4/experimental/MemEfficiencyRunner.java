package com.luxoft.lab4.experimental;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.Runner;
import org.junit.runner.manipulation.Filter;
import org.junit.runner.manipulation.Filterable;
import org.junit.runner.manipulation.NoTestsRemainException;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.junit.runner.notification.RunNotifier;

/**
 * Created by aniamamam on 2014-04-24.
 */

public class MemEfficiencyRunner extends Runner implements Filterable {
    private final Class<?> testClass;
    private Description testSuiteDescription;

    List<EfficiencyTest> testParameters = new ArrayList<>();

    public MemEfficiencyRunner(Class<?> testClass) {
        this.testClass = testClass;
        testSuiteDescription = Description.createSuiteDescription(testClass.getName());
        for (Method m : testClass.getMethods()) {
            EfficiencyTest testParameter = m.getAnnotation(EfficiencyTest.class);
            if (testParameter != null) {
                System.err.println("Found test for method "+m.getName());
                testSuiteDescription.addChild(Description.createTestDescription(testClass, m.getName()));
                testParameters.add(testParameter);
            }
        }
    }


    @Override
    public Description getDescription() {
        return testSuiteDescription;
    }

    @Override
    public void run(RunNotifier notifier) {
        System.err.println("RUN!!");
        notifier.fireTestRunStarted(testSuiteDescription);
        ArrayList<Description> children = testSuiteDescription.getChildren();
        Result result = new Result();
        RunListener listener = result.createListener();
        for (int i = 0; i < children.size(); i++) {
            Description description = children.get(i);
            notifier.fireTestStarted(description);
            try {
                System.err.println("Running thest for method "+description.getMethodName());
                runJUnit(testParameters.get(i), description.getMethodName());
                System.err.println("Finished thest for method "+description.getMethodName());
                if (false) {// TODO implement ignore
                    notifier.fireTestIgnored(description);
                    listener.testIgnored(description);
                } else {
                    notifier.fireTestFinished(description);
                    listener.testFinished(description);
                }
            } catch (Exception e) {
                Failure failure = new Failure(description, e);
                notifier.fireTestFailure(failure);
            }
        }
    }

    private void runJUnit(EfficiencyTest efficiencyTest, String methodName) throws Exception {
        String cp = System.getProperty("java.class.path");
        System.err.println(cp);
        Process p = new ProcessBuilder()
                .command("java", "-cp", cp, "-Xmx"+efficiencyTest.heapMaxMb()+"M", getClass().getCanonicalName(), testClass.getCanonicalName(), methodName)
                .redirectErrorStream(true)
                .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                .start();
        int result = p.waitFor();
        if (result != 0) {
            throw new AssertionError("SubProcess for test didn't finish properly.");
        }
    }

    private static Object reflectMeATestContainingInstance(Class<?> testClass) {
        try {
            return testClass.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Description createTestDescription(String description) {
        return Description.createTestDescription(testClass, description);
    }

    public static void main(String[] args) throws Exception {
        String testClassName = args[0];
        String methodName = args[1];
        Class<?> testClass = Class.forName(testClassName);
        Method method = testClass.getMethod(methodName);
        Object testContainingInstance = reflectMeATestContainingInstance(testClass);
        method.invoke(testContainingInstance);
    }

    @Override
    public void filter(Filter filter) throws NoTestsRemainException {
        // TODO implement me
    }
}