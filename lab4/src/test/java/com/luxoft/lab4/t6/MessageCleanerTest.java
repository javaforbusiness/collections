package com.luxoft.lab4.t6;

import java.util.Random;

import org.junit.Test;

import com.luxoft.lab4.utils.EfficiencyTestUtil;

/**
 * Created by aniamamam on 2014-04-23.
 */

public class MessageCleanerTest extends EfficiencyTestUtil {
    private static Random random = new Random(123456L);

    @Test
    public void testRunTimeEasy() throws Exception {
        expectMaxTimeWithNumberOfMessages(1000, 100000);

    }

    @Test
    public void testMemEfficiencyEasy() throws Exception {
        expectMaxMemoryWithNumberOfMessages(56, 1000000);
    }

    @Test
    public void testMemEfficiencyIntermediate() throws Exception {
        expectMaxMemoryWithNumberOfMessages(32, 1000000);
    }

    @Test
    public void testMemEfficiencyExpert() throws Exception {
        expectMaxMemoryWithNumberOfMessages(29, 1000000);
    }

    @Test
    public void testMemEfficiencyMaster() throws Exception {
        expectMaxMemoryWithNumberOfMessages(12, 1000000);
    }

    private void expectMaxTimeWithNumberOfMessages(int timeInMsec, int numberOfMessages) throws Exception {
        runTestTimeConstraint(timeInMsec, String.valueOf(numberOfMessages));
    }

    private void expectMaxMemoryWithNumberOfMessages(int memory, int numberOfMessages) throws Exception {
        runTestMemConstraint(memory, String.valueOf(numberOfMessages));
    }


    public static void main(String[] args) {
        int[] testIntegers = createTestIntegers();

        MessagesIds messageIds = new MessageIdsListImpl();

        for (int i : testIntegers) {
            messageIds.addId(i);
        }

        MessageCleaner cleaner = new MessageCleaner();
        cleaner.setMessageIds(messageIds);
        cleaner.setMessageDbStore(new TestDbStore(testIntegers));
        cleaner.cleanup();

    }


    private static int[] createTestIntegers() {
        int[] integers = new int[1000000];
        for (int i = 0; i < integers.length; i++) {
            integers[i] = random.nextInt();
        }
        return integers;
    }

    private static class TestDbStore implements MessageDbStore {
        private final int[] testIntegers;
        private int pos = 0;

        TestDbStore(int[] testIntegers) {
            this.testIntegers = testIntegers;
        }

        @Override
        public void cleanupMessage(int messageId) {
            int expectedInteger = testIntegers[pos++];
            if (expectedInteger != messageId) {
                throw new AssertionError("Expected to cleanup messageId=" + expectedInteger + " but " + messageId + " found.");
            }
        }
    }
}
